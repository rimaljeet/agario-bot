#include "bot.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double myX, myY,randv;
int start;
int virusvar = 0;

int dist(const void*a, const void *b)
{
    double ax = ((struct food *)a)-> x;
    double ay = ((struct food *)a)-> y;
    double distance1 = sqrt((myX - ax)*(myX-ax) + (myY - ay)*(myY- ay));
    
    double bx = ((struct food *)b)-> x;
    double by = ((struct food *)b)-> y;
    double distance2 = sqrt((myX - bx)*(myX-bx) + (myY - by)*(myY- by));
    
    return distance1 - distance2;
}

double myXp, myYp;
int distplayer(const void *a, const void *b)
{
    double ax = ((struct player *)a)-> x;
    double ay = ((struct player *)a)-> y;
    double distap = sqrt((myXp -ax)*(myXp -ax) + (myYp-ay)*(myYp-ay) );
    
    double bx = ((struct player *)b)-> x;
    double by = ((struct player *)b)-> y;
    double distbp = sqrt((myXp -bx)*(myXp -bx) + (myYp-by)*(myYp-by) );
    
    return (distap - distbp);
}

int green(const void *a,const void *b)
{
    double ax2 = ((struct cell *)a) -> x;
    double ay2 = ((struct cell *)a) -> y;
    double distap1 = sqrt((myXp -ax2)*(myXp -ax2) + (myYp-ay2)*(myYp-ay2) );
    
    double bx2 = ((struct cell *)b) -> x;
    double by2 = ((struct cell *)b) -> y;
    double distbp2 = sqrt((myXp -bx2)*(myXp -bx2) + (myYp-by2)*(myYp-by2) );
    
    return distap1 - distbp2;
    
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    myX = me.x;
    myY = me.y;
    
    if(start == 0 && start != 10)
    {
        randv = 10;
        act.dx = randv;
        start++; 
    }
    
    qsort(foods,nfood, sizeof(struct food), dist);
    qsort(players,nplayers,sizeof(struct player),distplayer);
    qsort(virii,nvirii,sizeof(struct cell),green);
    
    double playervar = 10;
    double count = 0;
    
    if(abs(players[0].x - myX) > 200)
    {
        act.dx = (foods[0].x - myX) * 1000;
        act.dy = (foods[0].y - myY) * 1000;
        act.fire = 0;
        act.split = 0;
        printf("food");
    }
    if(abs(players[0].x - myX) <= 10)
    {
        //printf("%d",pc);
        act.dx = (players[0].x + playervar);
        act.dy = (players[0].y + playervar);
        
        count++;
        
        if(count <= 2000)
        {
            playervar = -1 * playervar;
            count = 0; 
        }
        printf("player");
    } 
    return act;
 
}
